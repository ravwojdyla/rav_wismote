#include "contiki.h"
#include "cfs/cfs.h"
#include "file_man.h"

#include <stdio.h>



/* File manager is abe to:
 * 1) create file
 * 2) create directory
 * 3) remove file
 * 4) remove directory
 * 5) write/append to file
 * 6) read from file
 */

#define DEBUG 1

#ifdef DEBUG
    #define PRINTF(...) printf(__VA_ARGS__)
#endif

/* -------------------------------------------- */
PROCESS(file_manager, "File manager");
AUTOSTART_PROCESSES(&file_manager);
/* -------------------------------------------- */

PROCESS_THREAD(file_manager, ev, data)
{
  PROCESS_BEGIN();

  PRINTF("File manager protothread starts ...");

  /* step 1 */
  char message[32];
  char buf[100];

  strcpy(message,"#1.hello world.");
  strcpy(buf,message);
  printf("step 1: %s\n", buf );

  /* End Step 1. We will add more code below this comment later */

  /* step 2 */
  /* writing to cfs */
  char *filename = "msg_file/fds";
  int fd_write, fd_read;
  int n;

  struct cfs_dir* dirp = (struct cfs_dir*)malloc(sizeof(struct cfs_dir));
      cfs_opendir(dirp, "fds");
/*

  fd_write = cfs_open(filename, CFS_WRITE);
  if(fd_write != -1) {
    n = cfs_write(fd_write, message, sizeof(message));
    cfs_close(fd_write);
    printf("step 2: successfully written to cfs. wrote %i bytes\n", n);
  } else {
    printf("ERROR: could not write to memory in step 2.\n");
  }*/

  PROCESS_END();
}
