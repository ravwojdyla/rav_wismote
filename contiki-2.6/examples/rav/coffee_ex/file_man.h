/*
 * file_man.h
 *
 *  Created on: Aug 7, 2012
 *      Author: user
 */

#ifndef FILE_MAN_H_
#define FILE_MAN_H_

/* File manager events declarations: */
#define FM_OPEN_EVENT     0x20 /* Open of {file, dir} event. */
#define FM_CREATE_EVENT   0x21 /* Create of {file, dir} event. */
#define FM_REMOVE_EVENT   0x22 /* Remove of {file, dir} event. */
#define FM_WRITE_EVENT    0x23 /* Write to {file} event. */
#define FM_READ_EVENT     0x24 /* Read from {file, dir} event. */

/* Provide access to current dir of file manager. */
#define CURRENT_DIR() current_dir;

/* Additional flags for specifying that operation will work on directory. */
#define FM_DIR 0x08

/* Will create {file, dir} in path provided by argument name.
 * Flags argument specify if it's about file or directory.
 *
 * Returns:
 *      0 or -1 if it was able to create {file, dir}.
 *      file descriptor if everything was OK.
 */
int fmcreate(const char *name, int flags);

/* Will open {file, dir} specified by argument name.
 * Flags argument specify if it's about file or directory.
 *
 * Returns:
 *      0 or -1 if it was able to open {file, dir}.
 *      file descriptor if everything was OK.

int fmopen(const char *name, int flags);*/

/* Will remove {file, dir} specified by argument name.
 * Flags argument specify if it's about file or directory.
 *
 * Returns:
 *      -1 if it was able to remove {file, dir}.
 *      0 if everything was OK.
 */
int fmremove(const char* name, int flags);

int fmread(const char* name, void *buf, unsigned int len);
int fmwrite(const char* name, void* buf, unsigned int len);



#endif /* FILE_MAN_H_ */
