#include "contiki.h"
#include <stdio.h>

#define INT_MINIMUM –2147483648
#define EVENTS_EQALS(ev1, ev2) ((ev1) == (ev2))
#define MEAN_COMPUTED_EVENT 0x0a
#define MAX_SET 10

static int sets[][MAX_SET] = { { 2 }, /* Number of sets */
                             { 3, 1, 2, 3 }, /* First number is a number of elements in set */
                             { 5, 5453, 32, 343, 435, 43 } };

#define NUMBER_OF_SETS sets[0][0]

/* TASKS:
 * 1. add another event in case of bad computation of mean
 * 2. add handling of that new event
 * 3. and handling of case when there was no result in finding the biggest mean
 * 4. celebrate :D
 */


/*---------------------------------------------------------------------------*/
PROCESS(find_biggest_mean, "Finds mean of sets.");
PROCESS(find_mean_of_set, "Finds mean of set.");

AUTOSTART_PROCESSES(&find_biggest_mean);
/*---------------------------------------------------------------------------*/

PROCESS_THREAD(find_biggest_mean, ev, data)
{
    PROCESS_BEGIN();

        printf("Find biggest means starts ... ");

        static int counted_means = 0;
        static int biggest_so_far = 0;
        int i = 0;

        /* Start counting process, it will YIELD right after start, and
         * wait for event continue with data pointer to set of which it will
         * count mean, and post event back to this process with counted mean.
         */
        process_start(&find_mean_of_set, NULL);

        /* For each set we are sending continue event with data argument pointing to
         * instance of set.
         */
        for(i = 1; i <= NUMBER_OF_SETS; ++i)
            process_post(&find_mean_of_set, PROCESS_EVENT_CONTINUE,
                         (process_data_t)sets[i]);

        while(1) {

            /*      do
             {
             PROCESS_YIELD();
             } while(!EVENTS_EQALS(MEAN_COMPUTED_EVENT, ev));*/

            /* Q: Has the commented code above the same in functionality as the
             *    line below?  Beside of setting PT_YIELD_FLAG.
             */
            /* A: Yes. */

            PROCESS_YIELD_UNTIL(EVENTS_EQALS(MEAN_COMPUTED_EVENT, ev));

            /*when we receive mean, increment counted_means var and check if counted
             * mean is bigger then the biggest so far, if it is then store it in var
             * biggest_so_far.
             */

            if(biggest_so_far < (int)data)
                biggest_so_far = (int)data;
            if(++counted_means == NUMBER_OF_SETS)
                break;
        }

        /* Even that we have all results, find_mean_of_set protothread is still
         * waiting for requests, as we finish this thread we can stop find_mean_of_set
         * as well. We can do that by sending PROCESS_EVENT_EXIT.
         */
        /* Q: In find_mean_of_set protothread will are not handling PROCESS_EVENT_EXIT
         *    event, why this will work?
         */
        /* A: Well the answer is in code. When event is handled in Contiki, it will
         *    call call_process with specific protothread, event and data.
         *    find_mean_of_set thread's function will be called with ev set to
         *    PROCESS_EVENT_EXIT, we will check it in PROCESS_WAIT_UNTIL and YIELD
         *    process as we are waiting for PROCESS_EVENT_CONTINUE. But in call_process
         *    function protothread function call checks whether event was PROCESS_EVENT_EXIT
         *    and if so it will exit protohread.
         */

        process_post(&find_mean_of_set, PROCESS_EVENT_EXIT, NULL);

        printf("The biggest mean is %d\n", biggest_so_far);

    PROCESS_END();
}
/*---------------------------------------------------------------------------*/

PROCESS_THREAD(find_mean_of_set, ev, data)
{
    int* set = NULL;
    int i = 0;
    unsigned long tmp = 0;
PROCESS_BEGIN()
    ;

    /* Q: Why computations variable has to be static ? */
    /* A: Because it's value has to be kept between each calls of process functions.
     *    This is the way we store state in protothreads, by declaring variables as
     *    static.
     */

    static int computations = 0;

    /* Q: What is the difference between PROCESS_YIELD_UNTIL and PROCESS_WAIT_UNITL
     * and why PROCESS_YIELD_UNTIL will not work here (if there is more then 1 set.
     */
    /* A: PROCESS_YIELD_UNTIL will YIELD (pause) the protothread, and check
     *    if event is PROCESS_EVENT_CONTINUE in next call, on the other hand
     *    PROCESS_WAIT_UNTIL will check if current event is PROCESS_EVENT_CONTINUE
     *    if not it will YIELD protothread, and if it is PROCESS_EVENT_CONTINUE
     *    it will keep on executing protothread function. Why it will not work
     *    here - well let's check the the calls schedule:
     *    1) process_start by find_biggest_mean - it will yield find_mean_of_set
     *       at the line below.
     *    2) first event (for the first set) will execute thread and PT_RESTART
     *       will restart it.
     *    3) thread is restarted so it would do execute every line of the thread
     *       function from the beginning - PROCESS_YIELD_UNTIL as well so it will
     *       YIELD - this way we will lose one event and one set, next event would
     *       be executed correctly.
     */
    PROCESS_WAIT_UNTIL(EVENTS_EQALS(PROCESS_EVENT_CONTINUE, ev));
    /* data is a pointer to array of int elements. First element is number of
     * elements in array.
     */

    set = (int*)data;

    for(i = 1; i <= set[0]; ++i) {
        tmp += (long)set[i];
    }

    if(set[0] > 0) {
        tmp /= set[0];
    }

    /* Q: What is the difference between process_post and process_post_synch?
     */
    /* A: process_post will post event and data on event queue and event will will
     *    (if protothread will be still running) handled later, basically it's
     *    process_post_asynch. process_post_synch will post event directly to
     *    protothread which if is running will handle event and after that we will
     *    return from process_post_synch.
     *    process_post will return right after posting event.
     *    process_post_synch will return after event will be handled by protothread.
     */
    process_post_synch(&find_biggest_mean, MEAN_COMPUTED_EVENT,
                       (process_data_t)tmp);

    printf("Current mean = %d. Computations done: %d\n", tmp, ++computations);

    PT_RESTART(&find_mean_of_set.pt);
    /* This will never happen, because we restart process in line above. The
     * only way to stop this process is to post PROCESS_EVENT_EXIT to it. call_process
     * function will stop this process.
     */
    /* Q: Why we have to add the line below? */
    /* A: Because the switch statement of thread function has to be complete.
     *    We could put here " } } " and beside of warnings about return value,
     *    and the fact that it's really bad code it would be OK :D
     */
PROCESS_END();
}
